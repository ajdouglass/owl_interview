﻿using System;

namespace ConsoleApp1
{
    internal interface IURLFactory
    {
        Uri GetUri(string line, string stopPointId);
    }
}