﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp1
{
    class TestAnnouncer : IAnnouncer
    {
        string arrivalFileName = "arrival_file.txt";
        string followingArrivalFileName = "following_arrival_file.txt";
        public TestAnnouncer()
        {
            using(StreamWriter w = File.AppendText(arrivalFileName))
            {
                w.Write($"{DateTime.Now.ToShortTimeString()} | Starting announcer" + Environment.NewLine);
            }
            using(StreamWriter w = File.AppendText(followingArrivalFileName))
            {
                w.Write($"{DateTime.Now.ToShortTimeString()} | Starting announcer" + Environment.NewLine);
            }

        }

        public void SendArrival(string arrival)
        {
            using(StreamWriter w = File.AppendText(arrivalFileName))
            {
                w.Write($"{DateTime.Now.ToShortTimeString()} | {arrival}" + Environment.NewLine);
            }
        }

        public void SendFollowingArrivals(IEnumerable<string> arrivals)
        {
            using(StreamWriter w = File.AppendText(followingArrivalFileName))
            {
                w.Write($"{DateTime.Now.ToShortTimeString()} ===================" + Environment.NewLine);
                foreach (var arrival in arrivals)
                {
                    w.Write($"{arrival}" + Environment.NewLine);
                }
            }
        }
    }
}
