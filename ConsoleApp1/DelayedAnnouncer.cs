﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    //Allows Announcements to be delayed by a given time
    class DelayedAnnouncer : IDelayedAnnouncer 
    {
        private IAnnouncer announcer;
        public DelayedAnnouncer(IAnnouncer announcer)
        {
            this.announcer = announcer;
        }

        public async Task ScheduleAnnouncement(string announcement, TimeSpan timeSpan)
        {
            Console.WriteLine($"scheduled to print {announcement} in {timeSpan.Minutes} minutes at {(DateTime.Now + timeSpan).ToShortTimeString()}");
            await Task.Delay(timeSpan);
            announcer.SendArrival(announcement);
        }

        public void SendArrival(string arrival)
        {
            this.announcer.SendArrival(arrival);
        }

        public void SendFollowingArrivals(IEnumerable<string> arrivals)
        {
            this.announcer.SendFollowingArrivals(arrivals);
        }
    }
}
