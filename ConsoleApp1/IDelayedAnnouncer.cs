﻿using System;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal interface IDelayedAnnouncer : IAnnouncer
    {
        Task ScheduleAnnouncement(string announcement, TimeSpan timeSpan);
    }
}