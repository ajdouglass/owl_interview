﻿using System;
using System.Net;

namespace ConsoleApp1
{
    class HttpClient : IHttpClient
    {
        public string Download(Uri uri)
        {
            using (WebClient wc = new WebClient())
            {
                return wc.DownloadString(uri);
            }
        }
    }
}
