﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    //Provides predictions by deserialising data from the TFL API
    class Predictor : IPredictor 
    {
        private readonly IURLFactory urlFactory;
        private readonly IHttpClient httpClient;
        
        public Predictor(IURLFactory urlFactory, IHttpClient httpClient)
        {
            this.urlFactory = urlFactory;
            this.httpClient = httpClient;
        }

        public List<Prediction> GetPredictions(string line, string stopPointId)
        {
            var data = httpClient.Download(urlFactory.GetUri(line, stopPointId));
            var predictions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Prediction>>(data);
            foreach (var p in predictions)
            {
                DateTime.SpecifyKind(p.expectedArrival, DateTimeKind.Utc);
            }
            return predictions;
        }
    }
}
