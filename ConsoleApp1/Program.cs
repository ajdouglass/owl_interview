﻿using System;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() != 2)
            {
                Console.WriteLine($"usage: {System.AppDomain.CurrentDomain.FriendlyName} line_id StopPoint_id");
            }
            var line = args[0];
            var stopPointId = args[1];

            var httpClient = new HttpClient();
            var urlFactory = new URLFactory();
            var predictor = new Predictor(urlFactory, httpClient);

            var delayedAnnouncer = new DelayedAnnouncer(new TestAnnouncer());

            var scheduler = new Scheduler(predictor, delayedAnnouncer);

            Console.WriteLine($"running scheduler: line {line} stop {stopPointId}");
            while (true)
            {
                scheduler.Run(line, stopPointId).Wait();
            }

            Console.ReadLine();
        }
    }
}
