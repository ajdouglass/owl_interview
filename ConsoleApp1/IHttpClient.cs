﻿using System;

namespace ConsoleApp1
{
    internal interface IHttpClient
    {
        string Download(Uri uri);
    }
}