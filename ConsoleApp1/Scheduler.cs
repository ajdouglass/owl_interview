﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    //Schedules future announcements based on predictions
    class Scheduler
    {
        private readonly IPredictor predictor;
        private readonly IDelayedAnnouncer delayedAnnouncer;

        public Scheduler(IPredictor predictor, IDelayedAnnouncer delayedAnnouncer)
        {
            this.predictor = predictor;
            this.delayedAnnouncer = delayedAnnouncer;
        }

        public async Task Run(string line, string stopPointId)
        {
            var predictions = this.predictor.GetPredictions(line, stopPointId);
            this.delayedAnnouncer.SendFollowingArrivals(predictions.Select(FutureArrival));
            await Task.WhenAll(predictions.Select(p => this.delayedAnnouncer.ScheduleAnnouncement(Announcement(p), GetDelay(p))));
        }

        private string Announcement(Prediction prediction)
        {
            return $"prediction {prediction.id} arriving";
        }

        private string FutureArrival(Prediction prediction)
        {
            return $"prediction {prediction.id} arriving at {prediction.expectedArrival.ToLocalTime().ToShortTimeString()} in {GetDelay(prediction).Minutes} minutes";
        }

        private TimeSpan GetDelay(Prediction prediction)
        {
            var predictionTIme = prediction.expectedArrival;
            return prediction.expectedArrival - DateTime.UtcNow;
        }
    }
}
