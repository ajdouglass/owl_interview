﻿using System.Collections.Generic;

namespace ConsoleApp1
{
    interface IAnnouncer
    {
        void SendArrival(string arrival);
        void SendFollowingArrivals(IEnumerable<string> arrivals);
    }
}
