﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;

namespace TestConsoleApp1
{
    [TestClass]
    public class TestUrlFactory
    {
        [TestMethod]
        public void URL_is_formatted_correctly()
        {
            Assert.AreEqual("https://api.tfl.gov.uk/Line/bakerloo/Arrivals/940GZZLUWLO?direction=inbound", 
                            new URLFactory().GetUri("bakerloo", "940GZZLUWLO").ToString());
        }
    }
}
