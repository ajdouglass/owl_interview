﻿using System.Collections.Generic;

namespace ConsoleApp1
{
    internal interface IPredictor
    {
        List<Prediction> GetPredictions(string line, string stopPointId);
    }
}