﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class URLFactory : IURLFactory
    {
        public Uri GetUri(string line, string stopPointId)
        {
            return new Uri($"https://api.tfl.gov.uk/Line/{line}/Arrivals/{stopPointId}?direction=inbound");
        }
    }
}
